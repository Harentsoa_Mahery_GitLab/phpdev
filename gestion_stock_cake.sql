-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               10.1.16-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5174
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping data for table gestion_stock_2017.approvisionnements: ~6 rows (approximately)
/*!40000 ALTER TABLE `approvisionnements` DISABLE KEYS */;
REPLACE INTO `approvisionnements` (`id`, `fournisseur_id`, `produit_id`, `qteentree`) VALUES
	(13, 2, 2, 1000),
	(14, 2, 2, 200),
	(15, 1, 1, 100),
	(16, 2, 2, 250),
	(17, 2, 1, 400),
	(18, 3, 13, 200);
/*!40000 ALTER TABLE `approvisionnements` ENABLE KEYS */;

-- Dumping data for table gestion_stock_2017.auditapprovisionnements: ~9 rows (approximately)
/*!40000 ALTER TABLE `auditapprovisionnements` DISABLE KEYS */;
REPLACE INTO `auditapprovisionnements` (`aa_ID`, `aa_event`, `aa_eventDate`, `aa_nomFrs`, `aa_designProd`, `aa_oldQteentree`, `aa_newQteentree`, `aa_utilisateur`) VALUES
	(2, 'INSERT', '2017-08-14 09:12:15', 'Fourniseur1', 'Biscuit', 57, 100, 'root@localhost'),
	(4, 'INSERT', '2017-08-14 09:12:20', 'Fournisseur2', 'Chocolat', 348, 25, 'root@localhost'),
	(5, 'UPDATE', '2017-05-31 23:56:34', 'Fournisseur2', 'Chocolat', 373, 250, 'root@localhost'),
	(6, 'UPDATE', '2017-06-01 00:06:25', 'Fourniseur1', 'Biscuit', 140, 12, 'root@localhost'),
	(7, 'UPDATE', '2017-06-01 00:10:09', 'Fournisseur2', 'Chocolat', 598, 1000, 'root@localhost'),
	(8, 'DELETE', '2017-06-01 00:14:06', 'Fourniseur1', 'Biscuit', 12, 0, 'root@localhost'),
	(9, 'INSERT', '2017-08-14 09:12:24', 'Fournisseur2', 'Biscuit', 112, 500, 'root@localhost'),
	(10, 'UPDATE', '2017-06-27 11:26:01', 'Fournisseur2', 'Biscuit', 612, 400, 'root@localhost'),
	(11, 'INSERT', '2017-08-14 09:12:29', 'Pilo', 'Cake', 1000, 200, 'root@localhost');
/*!40000 ALTER TABLE `auditapprovisionnements` ENABLE KEYS */;

-- Dumping data for table gestion_stock_2017.auditproduits: ~5 rows (approximately)
/*!40000 ALTER TABLE `auditproduits` DISABLE KEYS */;
REPLACE INTO `auditproduits` (`ap_ID`, `ap_event`, `ap_eventDate`, `ap_designProd`, `ap_oldStock`, `ap_newStock`, `ap_utilisateur`) VALUES
	(1, 'INSERT', '2017-08-11 16:30:47', 'Morvit', 0, 1800, 'root@localhost'),
	(2, 'UPDATE', '2017-08-11 17:11:13', 'Morvit', 3000, 3000, 'root@localhost'),
	(3, 'INSERT', '2017-08-11 17:11:25', 'Souris', 0, 50, 'root@localhost'),
	(4, 'UPDATE', '2017-08-11 17:11:30', 'Souris', 100, 100, 'root@localhost'),
	(6, 'INSERT', '2017-08-11 17:25:44', 'Test', 0, 4000, 'root@localhost');
/*!40000 ALTER TABLE `auditproduits` ENABLE KEYS */;

-- Dumping data for table gestion_stock_2017.auditventes: ~7 rows (approximately)
/*!40000 ALTER TABLE `auditventes` DISABLE KEYS */;
REPLACE INTO `auditventes` (`av_ID`, `av_event`, `av_eventDate`, `av_nomCli`, `av_designProd`, `av_oldQtesortie`, `av_newQtesortie`, `av_utilisateur`) VALUES
	(1, 'INSERT', '2017-05-31 23:39:23', 'Mahery', 'Biscuit', 157, 7, 'root@localhost'),
	(2, 'INSERT', '2017-05-31 23:40:10', 'Mahery', 'Biscuit', 150, 7, 'root@localhost'),
	(3, 'INSERT', '2017-05-31 23:40:52', 'Mahery', 'Biscuit', 143, 3, 'root@localhost'),
	(4, 'UPDATE', '2017-06-01 00:09:44', 'Mahery', 'Biscuit', 140, 30, 'root@localhost'),
	(5, 'INSERT', '2017-06-01 00:18:30', 'Harentsoa', 'Biscuit', 113, 1, 'root@localhost'),
	(6, 'DELETE', '2017-06-01 00:20:38', 'Harentsoa', 'Biscuit', 1, 0, 'root@localhost'),
	(7, 'INSERT', '2017-08-11 11:34:14', 'Mahery', 'Cake', 1200, 500, 'root@localhost');
/*!40000 ALTER TABLE `auditventes` ENABLE KEYS */;

-- Dumping data for table gestion_stock_2017.clients: ~4 rows (approximately)
/*!40000 ALTER TABLE `clients` DISABLE KEYS */;
REPLACE INTO `clients` (`id`, `nom`) VALUES
	(1, 'Mahery'),
	(2, 'Notoavina'),
	(3, 'Harentsoa'),
	(4, 'Piloconsult');
/*!40000 ALTER TABLE `clients` ENABLE KEYS */;

-- Dumping data for table gestion_stock_2017.fournisseurs: ~3 rows (approximately)
/*!40000 ALTER TABLE `fournisseurs` DISABLE KEYS */;
REPLACE INTO `fournisseurs` (`id`, `nom`) VALUES
	(1, 'Fourniseur1'),
	(2, 'Fournisseur2'),
	(3, 'Pilo');
/*!40000 ALTER TABLE `fournisseurs` ENABLE KEYS */;

-- Dumping data for table gestion_stock_2017.produits: ~7 rows (approximately)
/*!40000 ALTER TABLE `produits` DISABLE KEYS */;
REPLACE INTO `produits` (`id`, `designation`, `stock`) VALUES
	(1, 'Biscuit', 512),
	(2, 'Chocolat', 1498),
	(12, 'Farilac', 500),
	(13, 'Cake', 700),
	(14, 'Morvit', 3000),
	(15, 'Souris', 500),
	(17, 'Test', 4000);
/*!40000 ALTER TABLE `produits` ENABLE KEYS */;

-- Dumping data for table gestion_stock_2017.users: ~3 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
REPLACE INTO `users` (`id`, `username`, `password`, `path`) VALUES
	(3, 'aren', '$2y$10$1OWU8wrL6mOwtM9W36Z31u3n8E9eJivvA1OTMt23PJL', 'admin'),
	(4, 'hafa', '$2y$10$pMnMBBKNXtRsjkDDQwn6KeP5FwQxcaZdaGY5b2g/gdX', 'guest'),
	(5, 'root', '$2y$10$L4iEzmvR/6J9vBmmzd.3ze1uRlmQLikW92kHA6a7R8Y', 'admin');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping data for table gestion_stock_2017.ventes: ~4 rows (approximately)
/*!40000 ALTER TABLE `ventes` DISABLE KEYS */;
REPLACE INTO `ventes` (`id`, `produit_id`, `client_id`, `qtesortie`) VALUES
	(2, 2, 2, 12),
	(15, 1, 1, 7),
	(17, 1, 1, 30),
	(18, 13, 1, 500);
/*!40000 ALTER TABLE `ventes` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
