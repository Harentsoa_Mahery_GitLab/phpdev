<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Approvisionnements Controller
 *
 * @property \App\Model\Table\ApprovisionnementsTable $Approvisionnements
 *
 * @method \App\Model\Entity\Approvisionnement[] paginate($object = null, array $settings = [])
 */
class ApprovisionnementsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Fournisseurs', 'Produits']
        ];
        $approvisionnements = $this->paginate($this->Approvisionnements);

        $this->set(compact('approvisionnements'));
        $this->set('_serialize', ['approvisionnements']);
    }

    /**
     * View method
     *
     * @param string|null $id Approvisionnement id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $approvisionnement = $this->Approvisionnements->get($id, [
            'contain' => ['Fournisseurs', 'Produits']
        ]);

        $this->set('approvisionnement', $approvisionnement);
        $this->set('_serialize', ['approvisionnement']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $approvisionnement = $this->Approvisionnements->newEntity();
        if ($this->request->is('post')) {
            $approvisionnement = $this->Approvisionnements->patchEntity($approvisionnement, $this->request->getData());
            if ($this->Approvisionnements->save($approvisionnement)) {
                $this->Flash->success(__('The approvisionnement has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The approvisionnement could not be saved. Please, try again.'));
        }
        $fournisseurs = $this->Approvisionnements->Fournisseurs->find('list', ['limit' => 200]);
        $produits = $this->Approvisionnements->Produits->find('list', ['limit' => 200]);
        $this->set(compact('approvisionnement', 'fournisseurs', 'produits'));
        $this->set('_serialize', ['approvisionnement']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Approvisionnement id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $approvisionnement = $this->Approvisionnements->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $approvisionnement = $this->Approvisionnements->patchEntity($approvisionnement, $this->request->getData());
            if ($this->Approvisionnements->save($approvisionnement)) {
                $this->Flash->success(__('The approvisionnement has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The approvisionnement could not be saved. Please, try again.'));
        }
        $fournisseurs = $this->Approvisionnements->Fournisseurs->find('list', ['limit' => 200]);
        $produits = $this->Approvisionnements->Produits->find('list', ['limit' => 200]);
        $this->set(compact('approvisionnement', 'fournisseurs', 'produits'));
        $this->set('_serialize', ['approvisionnement']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Approvisionnement id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $approvisionnement = $this->Approvisionnements->get($id);
        if ($this->Approvisionnements->delete($approvisionnement)) {
            $this->Flash->success(__('The approvisionnement has been deleted.'));
        } else {
            $this->Flash->error(__('The approvisionnement could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
