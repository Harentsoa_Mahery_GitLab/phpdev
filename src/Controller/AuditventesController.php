<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Auditventes Controller
 *
 * @property \App\Model\Table\AuditventesTable $Auditventes
 *
 * @method \App\Model\Entity\Auditvente[] paginate($object = null, array $settings = [])
 */
class AuditventesController extends AppController
{

    public $paginate = [
        'limit' => 5,
        'order' => [
            'Auditvente.id' => 'asc'
        ]
    ];

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
    }


    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $auditventes = $this->paginate($this->Auditventes);

        $this->set(compact('auditventes'));
        $this->set('_serialize', ['auditventes']);
    }

    /**
     * View method
     *
     * @param string|null $id Auditvente id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $auditvente = $this->Auditventes->get($id, [
            'contain' => []
        ]);

        $this->set('auditvente', $auditvente);
        $this->set('_serialize', ['auditvente']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $auditvente = $this->Auditventes->newEntity();
        if ($this->request->is('post')) {
            $auditvente = $this->Auditventes->patchEntity($auditvente, $this->request->getData());
            if ($this->Auditventes->save($auditvente)) {
                $this->Flash->success(__('The auditvente has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The auditvente could not be saved. Please, try again.'));
        }
        $this->set(compact('auditvente'));
        $this->set('_serialize', ['auditvente']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Auditvente id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $auditvente = $this->Auditventes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $auditvente = $this->Auditventes->patchEntity($auditvente, $this->request->getData());
            if ($this->Auditventes->save($auditvente)) {
                $this->Flash->success(__('The auditvente has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The auditvente could not be saved. Please, try again.'));
        }
        $this->set(compact('auditvente'));
        $this->set('_serialize', ['auditvente']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Auditvente id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $auditvente = $this->Auditventes->get($id);
        if ($this->Auditventes->delete($auditvente)) {
            $this->Flash->success(__('The auditvente has been deleted.'));
        } else {
            $this->Flash->error(__('The auditvente could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
