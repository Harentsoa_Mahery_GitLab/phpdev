<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Ventes Controller
 *
 * @property \App\Model\Table\VentesTable $Ventes
 *
 * @method \App\Model\Entity\Vente[] paginate($object = null, array $settings = [])
 */
class VentesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Produits', 'Clients']
        ];
        $ventes = $this->paginate($this->Ventes);

        $this->set(compact('ventes'));
        $this->set('_serialize', ['ventes']);
    }

    /**
     * View method
     *
     * @param string|null $id Vente id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $vente = $this->Ventes->get($id, [
            'contain' => ['Produits', 'Clients']
        ]);

        $this->set('vente', $vente);
        $this->set('_serialize', ['vente']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $vente = $this->Ventes->newEntity();
        if ($this->request->is('post')) {
            $vente = $this->Ventes->patchEntity($vente, $this->request->getData());
            if ($this->Ventes->save($vente)) {
                $this->Flash->success(__('The vente has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The vente could not be saved. Please, try again.'));
        }
        $produits = $this->Ventes->Produits->find('list', ['limit' => 200]);
        $clients = $this->Ventes->Clients->find('list', ['limit' => 200]);
        $this->set(compact('vente', 'produits', 'clients'));
        $this->set('_serialize', ['vente']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Vente id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $vente = $this->Ventes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $vente = $this->Ventes->patchEntity($vente, $this->request->getData());
            if ($this->Ventes->save($vente)) {
                $this->Flash->success(__('The vente has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The vente could not be saved. Please, try again.'));
        }
        $produits = $this->Ventes->Produits->find('list', ['limit' => 200]);
        $clients = $this->Ventes->Clients->find('list', ['limit' => 200]);
        $this->set(compact('vente', 'produits', 'clients'));
        $this->set('_serialize', ['vente']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Vente id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $vente = $this->Ventes->get($id);
        if ($this->Ventes->delete($vente)) {
            $this->Flash->success(__('The vente has been deleted.'));
        } else {
            $this->Flash->error(__('The vente could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
