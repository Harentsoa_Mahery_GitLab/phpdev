<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Auditapprovisionnements Controller
 *
 * @property \App\Model\Table\AuditapprovisionnementsTable $Auditapprovisionnements
 *
 * @method \App\Model\Entity\Auditapprovisionnement[] paginate($object = null, array $settings = [])
 */
class AuditapprovisionnementsController extends AppController
{

   // personnaliser la pagination
    public $paginate = [
        //'fields' => ['Auditapprovisionnement.aa_Event', 'Auditapprovisionnement.aa_EventDate'],
        'limit' => 5,
        'order' => [
            'Auditapprovisionnement.id' => 'asc'
        ]
    ];



    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $auditapprovisionnements = $this->paginate($this->Auditapprovisionnements);

        $this->set(compact('auditapprovisionnements'));
        $this->set('_serialize', ['auditapprovisionnements']);
    }

    /**
     * View method
     *
     * @param string|null $id Auditapprovisionnement id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $auditapprovisionnement = $this->Auditapprovisionnements->get($id, [
            'contain' => []
        ]);

        $this->set('auditapprovisionnement', $auditapprovisionnement);
        $this->set('_serialize', ['auditapprovisionnement']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $auditapprovisionnement = $this->Auditapprovisionnements->newEntity();
        if ($this->request->is('post')) {
            $auditapprovisionnement = $this->Auditapprovisionnements->patchEntity($auditapprovisionnement, $this->request->getData());
            if ($this->Auditapprovisionnements->save($auditapprovisionnement)) {
                $this->Flash->success(__('The auditapprovisionnement has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The auditapprovisionnement could not be saved. Please, try again.'));
        }
        $this->set(compact('auditapprovisionnement'));
        $this->set('_serialize', ['auditapprovisionnement']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Auditapprovisionnement id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $auditapprovisionnement = $this->Auditapprovisionnements->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $auditapprovisionnement = $this->Auditapprovisionnements->patchEntity($auditapprovisionnement, $this->request->getData());
            if ($this->Auditapprovisionnements->save($auditapprovisionnement)) {
                $this->Flash->success(__('The auditapprovisionnement has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The auditapprovisionnement could not be saved. Please, try again.'));
        }
        $this->set(compact('auditapprovisionnement'));
        $this->set('_serialize', ['auditapprovisionnement']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Auditapprovisionnement id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $auditapprovisionnement = $this->Auditapprovisionnements->get($id);
        if ($this->Auditapprovisionnements->delete($auditapprovisionnement)) {
            $this->Flash->success(__('The auditapprovisionnement has been deleted.'));
        } else {
            $this->Flash->error(__('The auditapprovisionnement could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }


    /*
     * rechercher un auditApprov selon sont type d'action et le produit à fournir */
    public function actionned($aa_event)
    {
        //$this->loadModel('')
        $this->loadModel('Auditapprovisionnements');

        $auditapprovisionnement = $this->request->getParam('pass');
        $data = $this->Auditapprovisionnements->query('SELECT * FROM auditapprovisionnements aa')->where(['aa_event =' => $aa_event]);

        $this->set('data', $data);
        $this->render('index');

    }
}
