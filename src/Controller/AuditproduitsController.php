<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Auditproduits Controller
 *
 * @property \App\Model\Table\AuditproduitsTable $Auditproduits
 *
 * @method \App\Model\Entity\Auditproduit[] paginate($object = null, array $settings = [])
 */
class AuditproduitsController extends AppController
{

    public $paginate = [
        'limit' => 5,
        'order' => [
            'Auditproduit.id' => 'asc'
        ]
    ];

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
    }



    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $auditproduits = $this->paginate($this->Auditproduits);

        $this->set(compact('auditproduits'));
        $this->set('_serialize', ['auditproduits']);
    }

    /**
     * View method
     *
     * @param string|null $id Auditproduit id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $auditproduit = $this->Auditproduits->get($id, [
            'contain' => []
        ]);

        $this->set('auditproduit', $auditproduit);
        $this->set('_serialize', ['auditproduit']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $auditproduit = $this->Auditproduits->newEntity();
        if ($this->request->is('post')) {
            $auditproduit = $this->Auditproduits->patchEntity($auditproduit, $this->request->getData());
            if ($this->Auditproduits->save($auditproduit)) {
                $this->Flash->success(__('The auditproduit has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The auditproduit could not be saved. Please, try again.'));
        }
        $this->set(compact('auditproduit'));
        $this->set('_serialize', ['auditproduit']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Auditproduit id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $auditproduit = $this->Auditproduits->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $auditproduit = $this->Auditproduits->patchEntity($auditproduit, $this->request->getData());
            if ($this->Auditproduits->save($auditproduit)) {
                $this->Flash->success(__('The auditproduit has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The auditproduit could not be saved. Please, try again.'));
        }
        $this->set(compact('auditproduit'));
        $this->set('_serialize', ['auditproduit']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Auditproduit id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $auditproduit = $this->Auditproduits->get($id);
        if ($this->Auditproduits->delete($auditproduit)) {
            $this->Flash->success(__('The auditproduit has been deleted.'));
        } else {
            $this->Flash->error(__('The auditproduit could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
