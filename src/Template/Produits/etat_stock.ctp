<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Produit[]|\Cake\Collection\CollectionInterface $produits
 */
?>

<article id="main">
    <header>
        <h2>Page etat des stocks</h2>
        <p>Page concernant un filtre sur l'etat des stock.</p>
    </header>
    <section class="wrapper style5">
        <div class="inner">



            <!-- CONTENU DU FILTRE -->
            <div class="produits index large-9 medium-8 columns content">
            <!-- Table -->
                <h3><?= __('ETAT DE STOCK : ')  ?></h3>
                <table cellpadding="0" cellspacing="0">
                    <thead>
                        <tr>
                            <th scope="col"><?= $this->Paginator->sort('designation') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('Etat stock') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('Mouvement')?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($etatStock as $etatStock): ?>
                            <tr>
                                <td><?= h($etatStock->produits['designation']) ?></td>
                                <td><?= h($etatStock->etat_stock) ?></td>
                                <td class="actions">
                                    <?= $this->Html->link(__('Mouvement'), ['action' => 'mouvementStock', $etatStock->produits['designation']]) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>

            <!-- END of Table -->


        </div>
    </section>
</article>
