<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Produits'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Approvisionnements'), ['controller' => 'Approvisionnements', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Approvisionnement'), ['controller' => 'Approvisionnements', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Ventes'), ['controller' => 'Ventes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Vente'), ['controller' => 'Ventes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="produits form large-9 medium-8 columns content">
    <?= $this->Form->create($produit) ?>
    <fieldset>
        <legend><?= __('Add Produit') ?></legend>
        <?php
            echo $this->Form->control('designation');
            echo $this->Form->control('stock');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
