<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Produit[]|\Cake\Collection\CollectionInterface $produits
 */
?>

<article id="main">
    <header>
        <h2>Page mouvement de stock</h2>
        <p>Page concernant un filtre sur les mouvements de stock.</p>
    </header>
    <section class="wrapper style5">
        <div class="inner">



            <!-- CONTENU -->

            <!-- Table -->
            <div class="produits index large-9 medium-8 columns content">
                <h3><?= __('MOUVEMENT DE STOCK : ')  ?> </h3>
                <hr/>

                <h4><?= __('ENTREE : ')  ?></h4>
                    <table cellpadding="0" cellspacing="0">
                        <thead>
                        <tr>
                            <th scope="col"><?= $this->Paginator->sort('Bon') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('Stock entre') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('Date')?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($mouvementStock as $mouvementStock): ?>
                            <tr>
                                <td><?= h($mouvementStock->approvisionnements['numero']) ?></td>
                                <td><?= h($mouvementStock->approvisionnements['qteentree']) ?></td>
                                <td><?= h($mouvementStock->approvisionnements['date']) ?></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>

                <br/>
                <br/>

                <h4><?= __('SORTIE : ')  ?></h4>
                <table cellpadding="0" cellspacing="0">
                    <thead>
                    <tr>
                        <th scope="col"><?= $this->Paginator->sort('Bon') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('Stock sortie') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('Date')?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($mouvementStock as $mouvementStock): ?>
                        <tr>
                            <td><?= h($mouvementStock->ventes['numero']) ?></td>
                            <td><?= h($mouvementStock->ventes['qtesortie']) ?></td>
                            <td><?= h($mouvementStock->ventes['date']) ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>





            </div>
            <!-- END of Table -->




        </div>
    </section>
</article>
