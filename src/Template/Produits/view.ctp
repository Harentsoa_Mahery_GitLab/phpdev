<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Produit $produit
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Produit'), ['action' => 'edit', $produit->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Produit'), ['action' => 'delete', $produit->id], ['confirm' => __('Are you sure you want to delete # {0}?', $produit->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Produits'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Produit'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Approvisionnements'), ['controller' => 'Approvisionnements', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Approvisionnement'), ['controller' => 'Approvisionnements', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Ventes'), ['controller' => 'Ventes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Vente'), ['controller' => 'Ventes', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="produits view large-9 medium-8 columns content">
    <h3><?= h($produit->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Designation') ?></th>
            <td><?= h($produit->designation) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($produit->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Stock') ?></th>
            <td><?= $this->Number->format($produit->stock) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Approvisionnements') ?></h4>
        <?php if (!empty($produit->approvisionnements)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Fournisseur Id') ?></th>
                <th scope="col"><?= __('Produit Id') ?></th>
                <th scope="col"><?= __('Qteentree') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($produit->approvisionnements as $approvisionnements): ?>
            <tr>
                <td><?= h($approvisionnements->id) ?></td>
                <td><?= h($approvisionnements->fournisseur_id) ?></td>
                <td><?= h($approvisionnements->produit_id) ?></td>
                <td><?= h($approvisionnements->qteentree) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Approvisionnements', 'action' => 'view', $approvisionnements->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Approvisionnements', 'action' => 'edit', $approvisionnements->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Approvisionnements', 'action' => 'delete', $approvisionnements->id], ['confirm' => __('Are you sure you want to delete # {0}?', $approvisionnements->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Ventes') ?></h4>
        <?php if (!empty($produit->ventes)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Produit Id') ?></th>
                <th scope="col"><?= __('Client Id') ?></th>
                <th scope="col"><?= __('Qtesortie') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($produit->ventes as $ventes): ?>
            <tr>
                <td><?= h($ventes->id) ?></td>
                <td><?= h($ventes->produit_id) ?></td>
                <td><?= h($ventes->client_id) ?></td>
                <td><?= h($ventes->qtesortie) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Ventes', 'action' => 'view', $ventes->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Ventes', 'action' => 'edit', $ventes->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Ventes', 'action' => 'delete', $ventes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ventes->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
