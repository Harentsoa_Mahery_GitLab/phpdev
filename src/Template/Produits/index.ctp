<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Produit[]|\Cake\Collection\CollectionInterface $produits
  */
?>

<article id="main">
    <header>
        <h2>Page Produits</h2>
        <p>Page concernant les produits du site.</p>
    </header>
    <section class="wrapper style5">
        <div class="inner">

            <ul class="actions">
                <li><?= $this->Html->link(__('New Products'), ['action' => 'add']) ?></li>
            </ul>


            <!-- Table -->
                <div class="produits index large-9 medium-8 columns content">
                    <h3><?= __('Produits') ?></h3>
                    <table cellpadding="0" cellspacing="0">
                        <thead>
                            <tr>
                                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('designation') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('stock') ?></th>
                                <th scope="col" class="actions"><?= __('Actions') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($produits as $produit): ?>
                            <tr>
                                <td><?= $this->Number->format($produit->id) ?></td>
                                <td><?= h($produit->designation) ?></td>
                                <td><?= $this->Number->format($produit->stock) ?></td>
                                <td class="actions">
                                    <?= $this->Html->link(__('View'), ['action' => 'view', $produit->id]) ?>
                                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $produit->id]) ?>
                                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $produit->id], ['confirm' => __('Are you sure you want to delete # {0}?', $produit->id)]) ?>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                    <div class="paginator">
                        <ul class="pagination">
                            <?= $this->Paginator->first('<< ' . __('first')) ?>
                            <?= $this->Paginator->prev('< ' . __('previous')) ?>
                            <?= $this->Paginator->numbers() ?>
                            <?= $this->Paginator->next(__('next') . ' >') ?>
                            <?= $this->Paginator->last(__('last') . ' >>') ?>
                        </ul>
                        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
                    </div>
                </div>
            <!-- END of Table -->

        </div>
    </section>
</article>
