<?php
/**
  * @var \App\View\AppView $this
  */
?>
<article id="main">
    <header>
        <h2>Page Ventes</h2>
        <p>Page concernant les ventes des produits.</p>
    </header>
    <section class="wrapper style5">
        <div class="inner">


            <div class="ventes form large-9 medium-8 columns content">
                <?= $this->Form->create($vente) ?>
                <fieldset>
                    <legend><?= __('Edit Vente') ?></legend>
                    <?php
                        echo $this->Form->control('numero');
                        echo $this->Form->control('produit_id', ['options' => $produits, 'empty' => true]);
                        echo $this->Form->control('client_id', ['options' => $clients, 'empty' => true]);
                        echo $this->Form->control('qtesortie');
                        echo $this->Form->control('date');
                    ?>
                </fieldset>
                <?= $this->Form->button(__('Submit')) ?>
                <?= $this->Form->end() ?>
            </div>

        </div>
    </section>
</article>


