<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Vente $vente
  */
?>

<article id="main">
    <header>
        <h2>Page Ventes</h2>
        <p>Page concernant les ventes des produits.</p>
    </header>
    <section class="wrapper style5">
        <div class="inner">



            <div class="ventes view large-9 medium-8 columns content">
                <h3><?= h($vente->id) ?></h3>
                <table class="vertical-table">
                    <tr>
                        <th scope="row"><?= __('Produit') ?></th>
                        <td><?= $vente->has('produit') ? $this->Html->link($vente->produit->designation, ['controller' => 'Produits', 'action' => 'view', $vente->produit->designation]) : '' ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Client') ?></th>
                        <td><?= $vente->has('client') ? $this->Html->link($vente->client->nom, ['controller' => 'Clients', 'action' => 'view', $vente->client->nom]) : '' ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Id') ?></th>
                        <td><?= $this->Number->format($vente->id) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Qtesortie') ?></th>
                        <td><?= $this->Number->format($vente->qtesortie) ?></td>
                    </tr>
                </table>
            </div>


        </div>
    </section>
</article>


