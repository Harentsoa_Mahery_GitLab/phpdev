<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Vente[]|\Cake\Collection\CollectionInterface $ventes
  */
?>

<article id="main">
    <header>
        <h2>Page Ventes</h2>
        <p>Page concernant les ventes des produits.</p>
    </header>
    <section class="wrapper style5">
        <div class="inner">

            <ul class="actions">
                <li><?= $this->Html->link(__('New Ventes'), ['action' => 'add']) ?></li>
            </ul>


            <div class="ventes index large-9 medium-8 columns content">
                <h3><?= __('Ventes') ?></h3>
                <table cellpadding="0" cellspacing="0">
                    <thead>
                        <tr>
                            <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('numero') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('produit_id') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('client_id') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('qtesortie') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('date') ?></th>
                            <th scope="col" class="actions"><?= __('Actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($ventes as $vente): ?>
                        <tr>
                            <td><?= $this->Number->format($vente->id) ?></td>
                            <td><?= h($vente->numero) ?></td>
                            <td><?= $vente->has('produit') ? $this->Html->link($vente->produit->designation, ['controller' => 'Produits', 'action' => 'view', $vente->produit->designation]) : '' ?></td>
                            <td><?= $vente->has('client') ? $this->Html->link($vente->client->nom, ['controller' => 'Clients', 'action' => 'view', $vente->client->nom]) : '' ?></td>
                            <td><?= $this->Number->format($vente->qtesortie) ?></td>
                            <td><?= h($vente->date) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['action' => 'view', $vente->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $vente->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $vente->id], ['confirm' => __('Are you sure you want to delete # {0}?', $vente->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <div class="paginator">
                    <ul class="pagination">
                        <?= $this->Paginator->first('<< ' . __('first')) ?>
                        <?= $this->Paginator->prev('< ' . __('previous')) ?>
                        <?= $this->Paginator->numbers() ?>
                        <?= $this->Paginator->next(__('next') . ' >') ?>
                        <?= $this->Paginator->last(__('last') . ' >>') ?>
                    </ul>
                    <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
                </div>
            </div>
            <!-- END OF Table -->

        </div>
    </section>
</article>

