<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Auditproduit[]|\Cake\Collection\CollectionInterface $auditproduits
  */
?>


<article id="main">
    <header>
        <h2>Page audits Produits</h2>
        <p>Page concernant les audits sur les actions à propos des produits.</p>
    </header>
    <section class="wrapper style5">
        <div class="inner">



            <!--Table -->
            <div class="auditproduits index large-9 medium-8 columns content">
                <h3><?= __('Auditproduits') ?></h3>
                <table cellpadding="0" cellspacing="0">
                    <thead>
                        <tr>
                            <th scope="col"><?= $this->Paginator->sort('ID') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('Event') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('Date') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('Produit') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('OldStock') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('NewStock') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('User') ?></th>
                            <th scope="col" class="actions"><?= __('Actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($auditproduits as $auditproduit): ?>
                        <tr>
                            <td><?= $this->Number->format($auditproduit->ap_ID) ?></td>
                            <td><?= h($auditproduit->ap_event) ?></td>
                            <td><?= h($auditproduit->ap_eventDate) ?></td>
                            <td><?= h($auditproduit->ap_designProd) ?></td>
                            <td><?= $this->Number->format($auditproduit->ap_oldStock) ?></td>
                            <td><?= $this->Number->format($auditproduit->ap_newStock) ?></td>
                            <td><?= h($auditproduit->ap_utilisateur) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['action' => 'view', $auditproduit->ap_ID]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <div class="paginator">
                    <ul class="pagination">
                        <?= $this->Paginator->first('<< ' . __('first')) ?>
                        <?= $this->Paginator->prev('< ' . __('previous')) ?>
                        <?= $this->Paginator->numbers() ?>
                        <?= $this->Paginator->next(__('next') . ' >') ?>
                        <?= $this->Paginator->last(__('last') . ' >>') ?>
                    </ul>
                    <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
                </div>
            </div>

            <!--END OF Table -->

        </div>
    </section>
</article>
