<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Auditproduit $auditproduit
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Auditproduit'), ['action' => 'edit', $auditproduit->ap_ID]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Auditproduit'), ['action' => 'delete', $auditproduit->ap_ID], ['confirm' => __('Are you sure you want to delete # {0}?', $auditproduit->ap_ID)]) ?> </li>
        <li><?= $this->Html->link(__('List Auditproduits'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Auditproduit'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="auditproduits view large-9 medium-8 columns content">
    <h3><?= h($auditproduit->ap_ID) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Ap Event') ?></th>
            <td><?= h($auditproduit->ap_event) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ap DesignProd') ?></th>
            <td><?= h($auditproduit->ap_designProd) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ap Utilisateur') ?></th>
            <td><?= h($auditproduit->ap_utilisateur) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ap ID') ?></th>
            <td><?= $this->Number->format($auditproduit->ap_ID) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ap OldStock') ?></th>
            <td><?= $this->Number->format($auditproduit->ap_oldStock) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ap NewStock') ?></th>
            <td><?= $this->Number->format($auditproduit->ap_newStock) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ap EventDate') ?></th>
            <td><?= h($auditproduit->ap_eventDate) ?></td>
        </tr>
    </table>
</div>
