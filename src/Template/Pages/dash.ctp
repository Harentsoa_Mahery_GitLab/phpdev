<!DOCTYPE HTML>
<!--
	Spectral by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
<head>
    <title>Transport</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!--[if lte IE 8]><script src="js/ie/html5shiv.js"></script><![endif]-->
    <link rel="stylesheet" href="css/main.css'" />
    <!--[if lte IE 8]><link rel="stylesheet" href="css/ie8.css" /><![endif]-->
    <!--[if lte IE 9]><link rel="stylesheet" href="css/ie9.css" /><![endif]-->
</head>
<body class="landing">

<!-- Page Wrapper -->
<div id="page-wrapper">

    <!-- Header -->
    <header id="header" class="alt">
        <h1><a href="index.html">Gestion Modulaire</a></h1>
        <nav id="nav">
            <ul>
                <li class="special">
                    <a href="#menu" class="menuToggle"><span>Menu</span></a>
                    <div id="menu">
                        <ul>
                            <li><a href="index.html"><span class="icon fa-cart-plus">Home</span></a></li>
                            <li><a href="cathegorie.html"><span class="icon fa-home">Client</span></a></li>
                            <li><a href="voiture.html"><span class="icon fa-car">Voiture</span></a></li>
                            <li><a href="voyage.html"><span class="icon fa-bus">Voyage</span></a></li>
                            <li><a href="client.html"><span class="icon fa-child">Client</span></a></li>
                            <li><a href="reglement.html"><span class="icon fa-money">Reglement</span></a></li>
                            <li><a href="reservation.html"><span class="icon fa-registered">Reservation</span></a></li>
                            <li><a href="billet.html"><span class="icon fa-ticket">Billet</span></a></li>
                        </ul>
                    </div>
                </li>
            </ul>
        </nav>
    </header>

    <!-- Banner -->
    <section id="banner">
        <div class="inner">
            <h2>Gestion Modulaire</h2>
            <p>Projet de getion <br />
                de transport<br />
                designed by <a href="http://www.facebook.com/ANJARANOTOAVINA.Harentsoa.Mahery/">Arentsu</a>.</p>
            <ul class="actions">
                <li><a href="#" class="button special">Bonjour</a></li>
            </ul>
        </div>
        <a href="#" class="more scrolly">Pourusivre</a>
    </section>

    <!-- Footer -->
    <footer id="footer">
        <ul class="icons">
            <li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
            <li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
            <li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
            <li><a href="#" class="icon fa-dribbble"><span class="label">Dribbble</span></a></li>
            <li><a href="#" class="icon fa-envelope-o"><span class="label">Email</span></a></li>
        </ul>
        <ul class="copyright">
            <li>&copy; G2M</li><li>Design: <a href="http://www.facebook.com/ANJARANOTOAVINA.Harentsoa.Mahery/">Arentsu</a></li>
        </ul>
    </footer>

</div>

<!-- Scripts -->
<script src="js/jquery.min.js"></script>
<script src="js/jquery.scrollex.min.js"></script>
<script src="js/jquery.scrolly.min.js"></script>
<script src="js/skel.min.js"></script>
<script src="js/util.js"></script>
<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
<script src="assets/js/main.js"></script>

</body>
</html>