<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Auditapprovisionnement[]|\Cake\Collection\CollectionInterface $auditapprovisionnements
  */
?>


<article id="main">
    <header>
        <h2>Page audits Approvisionnements</h2>
        <p>Page concernant les audits des approvisionnements des produits.</p>
    </header>
    <section class="wrapper style5">
        <div class="inner">

                <!-- Table -->
                <div class="auditapprovisionnements index large-9 medium-8 columns content">
                    <h3><?= __('Auditapprovisionnements') ?></h3>
                    <table cellpadding="0" cellspacing="0">
                        <thead>
                            <tr>
                                <th scope="col"><?= $this->Paginator->sort('ID') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('Event') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('Date') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('Fournisseur') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('Produit') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('Entree') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('Sortie') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('Users') ?></th>
                                <th scope="col" class="actions"><?= __('Actions') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($auditapprovisionnements as $auditapprovisionnement): ?>
                            <tr>
                                <td><?= $this->Number->format($auditapprovisionnement->aa_ID) ?></td>
                                <td><?= h($auditapprovisionnement->aa_event) ?></td>
                                <td><?= h($auditapprovisionnement->aa_eventDate) ?></td>
                                <td><?= h($auditapprovisionnement->aa_nomFrs) ?></td>
                                <td><?= h($auditapprovisionnement->aa_designProd) ?></td>
                                <td><?= $this->Number->format($auditapprovisionnement->aa_oldQteentree) ?></td>
                                <td><?= $this->Number->format($auditapprovisionnement->aa_newQteentree) ?></td>
                                <td><?= h($auditapprovisionnement->aa_utilisateur) ?></td>
                                <td class="actions">
                                    <?= $this->Html->link(__('View'), ['action' => 'view', $auditapprovisionnement->aa_ID]) ?>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                    <div class="paginator">
                        <ul class="pagination">
                            <?= $this->Paginator->first('<< ' . __('first')) ?>
                            <?= $this->Paginator->prev('< ' . __('previous')) ?>
                            <?= $this->Paginator->numbers() ?>
                            <?= $this->Paginator->next(__('next') . ' >') ?>
                            <?= $this->Paginator->last(__('last') . ' >>') ?>
                        </ul>
                    </div>
                </div>

            <!--END of Table -->

        </div>
    </section>
</article>

