<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Auditapprovisionnement $auditapprovisionnement
  */
?>
<article id="main">
    <header>
        <h2>Page audits Approvisionnements</h2>
        <p>Page concernant les audits des approvisionnements des produits.</p>
    </header>
    <section class="wrapper style5">
        <div class="inner">


                <div class="auditapprovisionnements view large-9 medium-8 columns content">
                    <h3><?= h($auditapprovisionnement->aa_ID) ?></h3>
                    <table class="vertical-table">
                        <tr>
                            <th scope="row"><?= __('ID') ?></th>
                            <td><?= $this->Number->format($auditapprovisionnement->aa_ID) ?></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= __('Event') ?></th>
                            <td><?= h($auditapprovisionnement->aa_event) ?></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= __('Date') ?></th>
                            <td><?= h($auditapprovisionnement->aa_eventDate) ?></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= __('Fournisseur') ?></th>
                            <td><?= h($auditapprovisionnement->aa_nomFrs) ?></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= __('Produit') ?></th>
                            <td><?= h($auditapprovisionnement->aa_designProd) ?></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= __('Utilisateur') ?></th>
                            <td><?= h($auditapprovisionnement->aa_utilisateur) ?></td>
                        </tr>

                        <tr>
                            <th scope="row"><?= __('OldEntree') ?></th>
                            <td><?= $this->Number->format($auditapprovisionnement->aa_oldQteentree) ?></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= __('NewEntree') ?></th>
                            <td><?= $this->Number->format($auditapprovisionnement->aa_newQteentree) ?></td>
                        </tr>

                    </table>
                </div>

        </div>
    </section>
</article>
