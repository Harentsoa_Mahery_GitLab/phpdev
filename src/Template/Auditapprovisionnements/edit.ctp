<?php
/**
  * @var \App\View\AppView $this
  */
?>
<article id="main">
    <header>
        <h2>Page audits Approvisionnements</h2>
        <p>Page concernant les audits des approvisionnements des produits.</p>
    </header>
    <section class="wrapper style5">
        <div class="inner">

            <nav class="large-3 medium-4 columns" id="actions-sidebar">
                <ul class="side-nav">
                    <li class="heading"><?= __('Actions') ?></li>
                    <li><?= $this->Form->postLink(
                            __('Delete'),
                            ['action' => 'delete', $auditapprovisionnement->aa_ID],
                            ['confirm' => __('Are you sure you want to delete # {0}?', $auditapprovisionnement->aa_ID)]
                        )
                    ?></li>
                    <li><?= $this->Html->link(__('List Auditapprovisionnements'), ['action' => 'index']) ?></li>
                </ul>
            </nav>
            <div class="auditapprovisionnements form large-9 medium-8 columns content">
                <?= $this->Form->create($auditapprovisionnement) ?>
                <fieldset>
                    <legend><?= __('Edit Auditapprovisionnement') ?></legend>
                    <?php
                        echo $this->Form->control('aa_event');
                        echo $this->Form->control('aa_eventDate');
                        echo $this->Form->control('aa_nomFrs');
                        echo $this->Form->control('aa_designProd');
                        echo $this->Form->control('aa_oldQteentree');
                        echo $this->Form->control('aa_newQteentree');
                        echo $this->Form->control('aa_utilisateur');
                    ?>
                </fieldset>
                <?= $this->Form->button(__('Submit')) ?>
                <?= $this->Form->end() ?>
            </div>

        </div>
    </section>
</article>

