<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Auditapprovisionnements'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="auditapprovisionnements form large-9 medium-8 columns content">
    <?= $this->Form->create($auditapprovisionnement) ?>
    <fieldset>
        <legend><?= __('Add Auditapprovisionnement') ?></legend>
        <?php
            echo $this->Form->control('aa_event');
            echo $this->Form->control('aa_eventDate');
            echo $this->Form->control('aa_nomFrs');
            echo $this->Form->control('aa_designProd');
            echo $this->Form->control('aa_oldQteentree');
            echo $this->Form->control('aa_newQteentree');
            echo $this->Form->control('aa_utilisateur');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
