<?php
/**
  * @var \App\View\AppView $this
  */
?>
<article id="main">
    <header>
        <h2>Page Approvisionnements</h2>
        <p>Page concernant l'ajout des approvisionnements des produits.</p>
    </header>
    <section class="wrapper style5">
        <div class="inner">

        <div class="approvisionnements form large-9 medium-8 columns content">
            <?= $this->Form->create($approvisionnement) ?>
            <fieldset>
                <legend><?= __('Add Approvisionnement') ?></legend>
                <?php
                    echo $this->Form->control('numero');
                    echo $this->Form->control('fournisseur_id', ['options' => $fournisseurs, 'empty' => true]);
                    echo $this->Form->control('produit_id', ['options' => $produits, 'empty' => true]);
                    echo $this->Form->control('qteentree');
                    echo $this->Form->control('date');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>

        </div>
    </section>
</article>