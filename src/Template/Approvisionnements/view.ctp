<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Approvisionnement $approvisionnement
  */
?>
<article id="main">
    <header>
        <h2>Page Approvisionnements</h2>
        <p>Page concernant les approvisionnemtns des produits.</p>
    </header>
    <section class="wrapper style5">
        <div class="inner">

            <div class="approvisionnements view large-9 medium-8 columns content">
                <h3><?= h($approvisionnement->id) ?></h3>
                <table class="vertical-table">
                    <tr>
                        <th scope="row"><?= __('Fournisseur') ?></th>
                        <td><?= $approvisionnement->has('fournisseur') ? $this->Html->link($approvisionnement->fournisseur->id, ['controller' => 'Fournisseurs', 'action' => 'view', $approvisionnement->fournisseur->id]) : '' ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Produit') ?></th>
                        <td><?= $approvisionnement->has('produit') ? $this->Html->link($approvisionnement->produit->id, ['controller' => 'Produits', 'action' => 'view', $approvisionnement->produit->id]) : '' ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Id') ?></th>
                        <td><?= $this->Number->format($approvisionnement->id) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Qteentree') ?></th>
                        <td><?= $this->Number->format($approvisionnement->qteentree) ?></td>
                    </tr>
                </table>
            </div>


        </div>
    </section>
</article>
