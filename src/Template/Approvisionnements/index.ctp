<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Approvisionnement[]|\Cake\Collection\CollectionInterface $approvisionnements
  */

?>



<article id="main">
    <header>
        <h2>Page Approvisionnements</h2>
        <p>Page concernant les approvisionnemtns des produits.</p>
    </header>
    <section class="wrapper style5">
        <div class="inner">

            <ul class="actions">
                <li><?= $this->Html->link(__('New Approvivisionnements'), ['action' => 'add']) ?></li>
            </ul>

                        <!--Table -->

                        <div class="approvisionnements index large-9 medium-8 columns content">
                            <h3><?= __('Approvisionnements') ?></h3>
                            <table cellpadding="0" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                                        <th scope="col"><?= $this->Paginator->sort('numero') ?></th>
                                        <th scope="col"><?= $this->Paginator->sort('fournisseur_id') ?></th>
                                        <th scope="col"><?= $this->Paginator->sort('produit_id') ?></th>
                                        <th scope="col"><?= $this->Paginator->sort('qteentree') ?></th>
                                        <th scope="col"><?= $this->Paginator->sort('date') ?></th>
                                        <th scope="col" class="actions"><?= __('Actions') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($approvisionnements as $approvisionnement): ?>
                                    <tr>
                                        <td><?= $this->Number->format($approvisionnement->id) ?></td>
                                        <td><?= h($approvisionnement->numero) ?></td>
                                        <td><?= $approvisionnement->has('fournisseur') ? $this->Html->link($approvisionnement->fournisseur->id, ['controller' => 'Fournisseurs', 'action' => 'view', $approvisionnement->fournisseur->id]) : '' ?></td>
                                        <td><?= $approvisionnement->has('produit') ? $this->Html->link($approvisionnement->produit->id, ['controller' => 'Produits', 'action' => 'view', $approvisionnement->produit->id]) : '' ?></td>
                                        <td><?= h($approvisionnement->qteentree) ?></td>
                                        <td><?= h($approvisionnement->date) ?></td>
                                        <td class="actions">
                                            <?= $this->Html->link(__('View'), ['action' => 'view', $approvisionnement->id]) ?>
                                            <?= $this->Html->link(__('Edit'), ['action' => 'edit', $approvisionnement->id]) ?>
                                            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $approvisionnement->id], ['confirm' => __('Are you sure you want to delete # {0}?', $approvisionnement->id)]) ?>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                            <div class="paginator">
                                <ul class="pagination">
                                    <?= $this->Paginator->first('<< ' . __('first')) ?>
                                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                                    <?= $this->Paginator->numbers() ?>
                                    <?= $this->Paginator->next(__('next') . ' >') ?>
                                    <?= $this->Paginator->last(__('last') . ' >>') ?>
                                </ul>
                                <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
                            </div>
                        </div>



        </div>
    </section>
</article>