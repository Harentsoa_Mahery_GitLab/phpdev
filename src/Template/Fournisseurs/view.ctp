<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Fournisseur $fournisseur
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Fournisseur'), ['action' => 'edit', $fournisseur->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Fournisseur'), ['action' => 'delete', $fournisseur->id], ['confirm' => __('Are you sure you want to delete # {0}?', $fournisseur->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Fournisseurs'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Fournisseur'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Approvisionnements'), ['controller' => 'Approvisionnements', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Approvisionnement'), ['controller' => 'Approvisionnements', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="fournisseurs view large-9 medium-8 columns content">
    <h3><?= h($fournisseur->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Nom') ?></th>
            <td><?= h($fournisseur->nom) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($fournisseur->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Approvisionnements') ?></h4>
        <?php if (!empty($fournisseur->approvisionnements)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Fournisseur Id') ?></th>
                <th scope="col"><?= __('Produit Id') ?></th>
                <th scope="col"><?= __('Qteentree') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($fournisseur->approvisionnements as $approvisionnements): ?>
            <tr>
                <td><?= h($approvisionnements->id) ?></td>
                <td><?= h($approvisionnements->fournisseur_id) ?></td>
                <td><?= h($approvisionnements->produit_id) ?></td>
                <td><?= h($approvisionnements->qteentree) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Approvisionnements', 'action' => 'view', $approvisionnements->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Approvisionnements', 'action' => 'edit', $approvisionnements->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Approvisionnements', 'action' => 'delete', $approvisionnements->id], ['confirm' => __('Are you sure you want to delete # {0}?', $approvisionnements->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
