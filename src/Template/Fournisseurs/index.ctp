<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Fournisseur[]|\Cake\Collection\CollectionInterface $fournisseurs
  */
?>

<article id="main">
    <header>
        <h2>Page Fournisseurs</h2>
        <p>Page concernant les fournisseurs des produits.</p>
    </header>
    <section class="wrapper style5">
        <div class="inner">

            <ul class="actions">
                <li><?= $this->Html->link(__('New Fournisseurs'), ['action' => 'add']) ?></li>
            </ul>


                    <!--Table -->
                    <div class="fournisseurs index large-9 medium-8 columns content">
                        <h3><?= __('Fournisseurs') ?></h3>
                        <table cellpadding="0" cellspacing="0">
                            <thead>
                                <tr>
                                    <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                                    <th scope="col"><?= $this->Paginator->sort('nom') ?></th>
                                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($fournisseurs as $fournisseur): ?>
                                <tr>
                                    <td><?= $this->Number->format($fournisseur->id) ?></td>
                                    <td><?= h($fournisseur->nom) ?></td>
                                    <td class="actions">
                                        <?= $this->Html->link(__('View'), ['action' => 'view', $fournisseur->id]) ?>
                                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $fournisseur->id]) ?>
                                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $fournisseur->id], ['confirm' => __('Are you sure you want to delete # {0}?', $fournisseur->id)]) ?>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                        <div class="paginator">
                            <ul class="pagination">
                                <?= $this->Paginator->first('<< ' . __('first')) ?>
                                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                                <?= $this->Paginator->numbers() ?>
                                <?= $this->Paginator->next(__('next') . ' >') ?>
                                <?= $this->Paginator->last(__('last') . ' >>') ?>
                            </ul>
                            <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
                        </div>
                    </div>

        <!--END of Table -->
        </div>
    </section>
</article>
