<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Auditvente[]|\Cake\Collection\CollectionInterface $auditventes
  */
?>

<article id="main">
<header>
    <h2>Page audit ventes</h2>
    <p>Page concernant les audits sur les ventes des produits.</p>
</header>
    <section class="wrapper style5">
        <div class="inner">


        <div class="auditventes index large-9 medium-8 columns content">
                <h3><?= __('Auditventes') ?></h3>
                <table cellpadding="0" cellspacing="0">
                    <thead>
                        <tr>
                            <th scope="col"><?= $this->Paginator->sort('ID') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('Event') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('Date') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('Client') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('Produit') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('OldSortie') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('NewSortie') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('User') ?></th>
                            <th scope="col" class="actions"><?= __('Actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($auditventes as $auditvente): ?>
                        <tr>
                            <td><?= $this->Number->format($auditvente->av_ID) ?></td>
                            <td><?= h($auditvente->av_event) ?></td>
                            <td><?= h($auditvente->av_eventDate) ?></td>
                            <td><?= h($auditvente->av_nomCli) ?></td>
                            <td><?= h($auditvente->av_designProd) ?></td>
                            <td><?= $this->Number->format($auditvente->av_oldQtesortie) ?></td>
                            <td><?= $this->Number->format($auditvente->av_newQtesortie) ?></td>
                            <td><?= h($auditvente->av_utilisateur) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['action' => 'view', $auditvente->av_ID]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <div class="paginator">
                    <ul class="pagination">
                        <?= $this->Paginator->first('<< ' . __('first')) ?>
                        <?= $this->Paginator->prev('< ' . __('previous')) ?>
                        <?= $this->Paginator->numbers() ?>
                        <?= $this->Paginator->next(__('next') . ' >') ?>
                        <?= $this->Paginator->last(__('last') . ' >>') ?>
                    </ul>
                    <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
                </div>
            </div>


        </div>
    </section>
</article>
