<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $auditvente->av_ID],
                ['confirm' => __('Are you sure you want to delete # {0}?', $auditvente->av_ID)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Auditventes'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="auditventes form large-9 medium-8 columns content">
    <?= $this->Form->create($auditvente) ?>
    <fieldset>
        <legend><?= __('Edit Auditvente') ?></legend>
        <?php
            echo $this->Form->control('av_event');
            echo $this->Form->control('av_eventDate');
            echo $this->Form->control('av_nomCli');
            echo $this->Form->control('av_designProd');
            echo $this->Form->control('av_oldQtesortie');
            echo $this->Form->control('av_newQtesortie');
            echo $this->Form->control('av_utilisateur');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
