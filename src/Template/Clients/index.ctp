<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Client[]|\Cake\Collection\CollectionInterface $clients
  */
?>
<article id="main">
    <header>
        <h2>Page Clients</h2>
        <p>Page concernant les clients ppouvant faire des commandes.</p>
    </header>
    <section class="wrapper style5">
        <div class="inner">

            <ul class="actions">
                <li><?= $this->Html->link(__('New Clients'), ['action' => 'add']) ?></li>
            </ul>

                <!-- Table -->
                <div class="clients index large-9 medium-8 columns content">
                    <h3><?= __('Clients') ?></h3>
                    <table cellpadding="0" cellspacing="0">
                        <thead>
                            <tr>
                                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('nom') ?></th>
                                <th scope="col" class="actions"><?= __('Actions') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($clients as $client): ?>
                            <tr>
                                <td><?= $this->Number->format($client->id) ?></td>
                                <td><?= h($client->nom) ?></td>
                                <td class="actions">
                                    <?= $this->Html->link(__('View'), ['action' => 'view', $client->id]) ?>
                                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $client->id]) ?>
                                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $client->id], ['confirm' => __('Are you sure you want to delete # {0}?', $client->id)]) ?>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                    <div class="paginator">
                        <ul class="pagination">
                            <?= $this->Paginator->first('<< ' . __('first')) ?>
                            <?= $this->Paginator->prev('< ' . __('previous')) ?>
                            <?= $this->Paginator->numbers() ?>
                            <?= $this->Paginator->next(__('next') . ' >') ?>
                            <?= $this->Paginator->last(__('last') . ' >>') ?>
                        </ul>
                        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
                    </div>
                </div>
                <!--END of Table -->

        </div>
    </section>
</article>


