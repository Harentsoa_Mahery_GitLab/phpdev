<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->css('main.css') ?>
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body class="landing">
<!-- Page Wrapper -->
<!-- Page Wrapper -->
<div id="page-wrapper">
    <!-- Page Wrapper -->
    <div id="page-wrapper">
            <!-- Header -->
            <header id="header">
                <h1><a href="index.html">STOCK MANAGEMENT</a></h1>
                <nav id="nav">
                    <ul>
                        <li class="special">
                            <a href="#menu" class="menuToggle"><span>Menu</span></a>
                            <div id="menu">
                                <ul>
                                    <li><a href="/cake_stock/users/dash"><span class="icon fa-home">Home</span></a></li>
                                    <li><a href="/cake_stock/clients"><span class="icon fa-envelope">clients</span></a></li>
                                    <li><a href="/cake_stock/produits"><span class="icon fa-car">Produits</span></a></li>
                                    <li><a href="/cake_stock/approvisionnements"><span class="icon fa-bicycle">Approvisionnements</span></a></li>
                                    <li><a href="/cake_stock/ventes"><span class="icon fa-money">Ventes</span></a></li>
                                    <li><a href="/cake_stock/auditapprovisionnements"><span class="icon fa-ticket">Audit Approvisionnement</span></a></li>
                                    <li><a href="/cake_stock/auditventes"><span class="icon fa-ticket">Audit Ventes</span></a></li>
                                    <li><a href="/cake_stock/auditproduits"><span class="icon fa-ticket">Audit Produits</span></a></li>
                                    <li><a href="/cake_stock/produits/etat_stock"><span class="icon fa-ticket">Etat de stock</span></a></li>
                                    <li><a href="/cake_stock/users"><span class="icon fa-ticket">Users</span></a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </nav>
            </header>


        <!-- Main -->




                <!-- Content -->
                        <?= $this->Flash->render() ?>
                        <div class="container clearfix">
                        <?= $this->fetch('content') ?>
                        </div>





        <!-- Footer -->
            <footer id="footer">
                <ul class="icons">
                    <li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
                    <li><a href="http://www.facebook.com/harentsoamahery.anjaranotoavina" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
                    <li><a href="http://www.instagram.com/harentsoamahery/" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
                    <li><a href="#" class="icon fa-dribbble"><span class="label">Dribbble</span></a></li>
                    <li><a href="#" class="icon fa-envelope-o"><span class="label">Email</span></a></li>
                </ul>
                <ul class="copyright">
                    <li>&copy; G2M</li><li>Design: <a href="http://www.facebook.com/harentsoamahery.anjaranotoavina">Arentsu</a></li>
                </ul>
            </footer>

        </div>

    </div>
        <!-- Scripts -->
        <?= $this->Html->script('jquery.min.js') ?>
        <?= $this->Html->script('jquery.scrollex.min.js') ?>
        <?= $this->Html->script('jquery.scrolly.min.js') ?>
        <?= $this->Html->script('skel.min.js') ?>
        <?= $this->Html->script('util.js') ?>
        <?= $this->Html->script('main.js') ?>
</body>
</html>
