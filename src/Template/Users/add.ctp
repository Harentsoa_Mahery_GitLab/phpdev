<?php
/**
  * @var \App\View\AppView $this
  */
?>

<article id="main">
    <header>
        <h2>Page Utilisateurs</h2>
        <p>Page concernant les utilisateurs du site.</p>
    </header>
    <section class="wrapper style5">
        <div class="inner">

            <h3>Add users</h3>


                <nav class="large-3 medium-4 columns" id="actions-sidebar">
                    <ul class="side-nav">
                        <li class="heading"><?= __('Actions') ?></li>
                        <li><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?></li>
                    </ul>
                </nav>
                <div class="users form large-9 medium-8 columns content">
                    <?= $this->Form->create($user) ?>
                    <fieldset>
                        <legend><?= __('Add User') ?></legend>
                        <?php
                            echo $this->Form->control('username');
                            echo $this->Form->control('password');
                            echo $this->Form->control('path',[
                                'options' => ['admin' => 'admin', 'guest' => 'guest']
                                ]);
                        ?>
                    </fieldset>
                    <?= $this->Form->button(__('Submit')) ?>
                    <?= $this->Form->end() ?>
                </div>


        </div>
    </section>
</article>

