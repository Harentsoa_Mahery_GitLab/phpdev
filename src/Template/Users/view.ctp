<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\User $user
  */
?>
<article id="main">
    <header>
        <h2>Page Utilisateurs</h2>
        <p>Page concernant les utilisateurs du site.</p>
    </header>
    <section class="wrapper style5">
        <div class="inner">

            <h3>View users</h3>

                        <div class="users view large-9 medium-8 columns content">
                            <h3><?= h($user->id) ?></h3>
                            <table class="vertical-table">
                                <tr>
                                    <th scope="row"><?= __('Username') ?></th>
                                    <td><?= h($user->username) ?></td>
                                </tr>
                                <tr>
                                    <th scope="row"><?= __('Password') ?></th>
                                    <td><?= h($user->password) ?></td>
                                </tr>
                                <tr>
                                    <th scope="row"><?= __('Path') ?></th>
                                    <td><?= h($user->path) ?></td>
                                </tr>
                                <tr>
                                    <th scope="row"><?= __('Id') ?></th>
                                    <td><?= $this->Number->format($user->id) ?></td>
                                </tr>
                            </table>
                        </div>
        </div>
    </section>
</article>
