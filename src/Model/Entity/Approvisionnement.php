<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Approvisionnement Entity
 *
 * @property int $id
 * @property int $fournisseur_id
 * @property int $produit_id
 * @property int $qteentree
 *
 * @property \App\Model\Entity\Fournisseur $fournisseur
 * @property \App\Model\Entity\Produit $produit
 */
class Approvisionnement extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
