<?php
namespace App\Model\Entity;

use Cake\Database\Query;
use Cake\ORM\Entity;

/**
 * Auditapprovisionnement Entity
 *
 * @property int $aa_ID
 * @property string $aa_event
 * @property \Cake\I18n\FrozenTime $aa_eventDate
 * @property string $aa_nomFrs
 * @property string $aa_designProd
 * @property int $aa_oldQteentree
 * @property int $aa_newQteentree
 * @property string $aa_utilisateur
 */
class Auditapprovisionnement extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'aa_ID' => false
    ];

}
