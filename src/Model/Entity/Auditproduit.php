<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Auditproduit Entity
 *
 * @property int $ap_ID
 * @property string $ap_event
 * @property \Cake\I18n\FrozenTime $ap_eventDate
 * @property string $ap_designProd
 * @property int $ap_oldStock
 * @property int $ap_newStock
 * @property string $ap_utilisateur
 */
class Auditproduit extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'ap_ID' => false
    ];
}
