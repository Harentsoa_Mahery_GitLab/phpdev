<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Auditvente Entity
 *
 * @property int $av_ID
 * @property string $av_event
 * @property \Cake\I18n\FrozenTime $av_eventDate
 * @property string $av_nomCli
 * @property string $av_designProd
 * @property int $av_oldQtesortie
 * @property int $av_newQtesortie
 * @property string $av_utilisateur
 */
class Auditvente extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'av_ID' => false
    ];
}
