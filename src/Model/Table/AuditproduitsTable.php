<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Auditproduits Model
 *
 * @method \App\Model\Entity\Auditproduit get($primaryKey, $options = [])
 * @method \App\Model\Entity\Auditproduit newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Auditproduit[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Auditproduit|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Auditproduit patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Auditproduit[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Auditproduit findOrCreate($search, callable $callback = null, $options = [])
 */
class AuditproduitsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('auditproduits');
        $this->setDisplayField('ap_ID');
        $this->setPrimaryKey('ap_ID');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('ap_ID')
            ->allowEmpty('ap_ID', 'create');

        $validator
            ->requirePresence('ap_event', 'create')
            ->notEmpty('ap_event');

        $validator
            ->dateTime('ap_eventDate')
            ->requirePresence('ap_eventDate', 'create')
            ->notEmpty('ap_eventDate');

        $validator
            ->requirePresence('ap_designProd', 'create')
            ->notEmpty('ap_designProd');

        $validator
            ->integer('ap_oldStock')
            ->requirePresence('ap_oldStock', 'create')
            ->notEmpty('ap_oldStock');

        $validator
            ->integer('ap_newStock')
            ->requirePresence('ap_newStock', 'create')
            ->notEmpty('ap_newStock');

        $validator
            ->requirePresence('ap_utilisateur', 'create')
            ->notEmpty('ap_utilisateur');

        return $validator;
    }
}
