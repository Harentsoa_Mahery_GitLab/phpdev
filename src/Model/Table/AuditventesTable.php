<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Auditventes Model
 *
 * @method \App\Model\Entity\Auditvente get($primaryKey, $options = [])
 * @method \App\Model\Entity\Auditvente newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Auditvente[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Auditvente|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Auditvente patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Auditvente[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Auditvente findOrCreate($search, callable $callback = null, $options = [])
 */
class AuditventesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('auditventes');
        $this->setDisplayField('av_ID');
        $this->setPrimaryKey('av_ID');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('av_ID')
            ->allowEmpty('av_ID', 'create');

        $validator
            ->requirePresence('av_event', 'create')
            ->notEmpty('av_event');

        $validator
            ->dateTime('av_eventDate')
            ->requirePresence('av_eventDate', 'create')
            ->notEmpty('av_eventDate');

        $validator
            ->requirePresence('av_nomCli', 'create')
            ->notEmpty('av_nomCli');

        $validator
            ->requirePresence('av_designProd', 'create')
            ->notEmpty('av_designProd');

        $validator
            ->integer('av_oldQtesortie')
            ->requirePresence('av_oldQtesortie', 'create')
            ->notEmpty('av_oldQtesortie');

        $validator
            ->integer('av_newQtesortie')
            ->requirePresence('av_newQtesortie', 'create')
            ->notEmpty('av_newQtesortie');

        $validator
            ->requirePresence('av_utilisateur', 'create')
            ->notEmpty('av_utilisateur');

        return $validator;
    }
}
