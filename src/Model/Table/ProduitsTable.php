<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Composer\Test\IO\NullIOTest;

/**
 * Produits Model
 *
 * @property \App\Model\Table\ApprovisionnementsTable|\Cake\ORM\Association\HasMany $Approvisionnements
 * @property \App\Model\Table\VentesTable|\Cake\ORM\Association\HasMany $Ventes
 *
 * @method \App\Model\Entity\Produit get($primaryKey, $options = [])
 * @method \App\Model\Entity\Produit newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Produit[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Produit|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Produit patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Produit[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Produit findOrCreate($search, callable $callback = null, $options = [])
 */
class ProduitsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('produits');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('Approvisionnements', [
            'foreignKey' => 'produit_id'
        ]);
        $this->hasMany('Ventes', [
            'foreignKey' => 'produit_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('designation', 'create')
            ->notEmpty('designation');

        $validator
            ->integer('stock')
            ->requirePresence('stock', 'create')
            ->notEmpty('stock');

        return $validator;
    }

/*
    public function findEtatStock2(Query $query) {
         $query = $this->find()
            ->select([
                'produits.designation',
                'etat_stock' => ($query->func()->sum('approvisionnements.qteentree') - $query->func()->sum('ventes.qtesortie'))
            ])
            ->join([
                'a' => [
                    'table' => 'approvisionnements',
                    'type' => 'INNER',
                    'conditions' => 'a.produit_id = proruits.id'
                ],
                'v' => [
                    'table' => 'ventes',
                    'type' => 'INNER',
                    'conditions' => 'v.produits_id = produits.id'
                ]
            ])
            ->where([['a.produit_id = produits.id'], ['v.produit_id = produits.id']]);
        return $query;
    }*/


    public function findEtatStock(Query $query){
        $query = $this->find()
            ->select([
                'produits.designation',
                'produits.stock',
                'approvisionnements.qteentree',
                'ventes.qtesortie',
                //'total_entree' => $query->func()->sum('approvisionnements.qteentree'),
                //'total_sortie' =>  $query->func()->sum('ventes.qtesortie') ,
                //'etat_stock' =>  total_entree - total_sortie
                'etat_stock' => 'stock + qteentree - qtesortie'



                //ref tsis trigger
                /****
                 *  SELECT p.Prod_design,
                 * (SELECT SUM( be.Be_qteEntree) FROM bondentree be WHERE be.ProduitProd_num = p.Prod_num GROUP BY p.Prod_num) - ( SELECT SUM( bs.Bs_qteSortie)
                FROM bondesortie bs WHERE bs.ProduitProd_num = p.Prod_num GROUP BY p.Prod_num ) AS etat_stock
                FROM produit p, bondentree be, bondesortie bs
                WHERE p.Prod_num = be.ProduitProd_num && p.Prod_num = bs.ProduitProd_num
                GROUP BY p.Prod_num
                 */

            ]);

            $query
                ->innerJoinWith('Approvisionnements')
                ->innerJoinWith('Ventes')
                ->where([
                    ['approvisionnements.produit_id = produits.id'], ['ventes.produit_id = produits.id']
                ])
                ->group('Produits.id');
        return $query;
    }

    public function findMouvementStock(Query $query, array $options){
        $query = $this->find()
            ->select([
                'produits.designation',
                'produits.stock',
                'approvisionnements.numreo',
                'approviionnements.date',
                'ventes.numero',
                'ventes.date',
                'approvisionnements.qteentree',
                'ventes.qtesortie',
                'total_entree' => $query->func()->sum('approvisionnements.qteentree'),
                'total_sortie' => $query->func()->sum('ventes.qtesortie')
            ]);
        if (empty($options['prod'])){
            $query
                ->innerJoinWith('Approvisionnements')
                ->innerJoinWith('Ventes')
                ->where([
                    'Produits.designation =' => NULL
                ]);
        }else{
            $query
                ->innerJoinWith('Approvisionnements')
                ->innerJoinWith('Ventes')
                ->where([
                    'Produits.designation IN ' => $options['prod']
                ]);
        }
        return $query;
    }
}
