<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

$auditapprovisionnements = TableRegistry::get('Auditapprovisionnements');



/**
 * Auditapprovisionnements Model
 *
 * @method \App\Model\Entity\Auditapprovisionnement get($primaryKey, $options = [])
 * @method \App\Model\Entity\Auditapprovisionnement newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Auditapprovisionnement[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Auditapprovisionnement|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Auditapprovisionnement patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Auditapprovisionnement[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Auditapprovisionnement findOrCreate($search, callable $callback = null, $options = [])
 */
class AuditapprovisionnementsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('auditapprovisionnements');
        $this->setDisplayField('aa_ID');
        $this->setPrimaryKey('aa_ID');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('aa_ID')
            ->allowEmpty('aa_ID', 'create');

        $validator
            ->requirePresence('aa_event', 'create')
            ->notEmpty('aa_event');

        $validator
            ->dateTime('aa_eventDate')
            ->requirePresence('aa_eventDate', 'create')
            ->notEmpty('aa_eventDate');

        $validator
            ->requirePresence('aa_nomFrs', 'create')
            ->notEmpty('aa_nomFrs');

        $validator
            ->requirePresence('aa_designProd', 'create')
            ->notEmpty('aa_designProd');

        $validator
            ->integer('aa_oldQteentree')
            ->requirePresence('aa_oldQteentree', 'create')
            ->notEmpty('aa_oldQteentree');

        $validator
            ->integer('aa_newQteentree')
            ->requirePresence('aa_newQteentree', 'create')
            ->notEmpty('aa_newQteentree');

        $validator
            ->requirePresence('aa_utilisateur', 'create')
            ->notEmpty('aa_utilisateur');

        return $validator;
    }


    //filter les actions selon le type d'action (C[R]UD)
    public function findAction(Query $query, array $options, $aa_event){

        $query = $this->Auditapprovisionnements->find('all')
                ->where(['aa_event' => $aa_event ]);
        return $query->group(['Auditapprovisionnements.id']);

    }
}
