<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AuditproduitsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AuditproduitsTable Test Case
 */
class AuditproduitsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AuditproduitsTable
     */
    public $Auditproduits;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.auditproduits'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Auditproduits') ? [] : ['className' => AuditproduitsTable::class];
        $this->Auditproduits = TableRegistry::get('Auditproduits', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Auditproduits);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
