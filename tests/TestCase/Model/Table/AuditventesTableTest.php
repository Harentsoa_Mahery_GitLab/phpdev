<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AuditventesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AuditventesTable Test Case
 */
class AuditventesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AuditventesTable
     */
    public $Auditventes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.auditventes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Auditventes') ? [] : ['className' => AuditventesTable::class];
        $this->Auditventes = TableRegistry::get('Auditventes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Auditventes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
