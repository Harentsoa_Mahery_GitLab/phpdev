<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AuditapprovisionnementsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AuditapprovisionnementsTable Test Case
 */
class AuditapprovisionnementsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AuditapprovisionnementsTable
     */
    public $Auditapprovisionnements;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.auditapprovisionnements'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Auditapprovisionnements') ? [] : ['className' => AuditapprovisionnementsTable::class];
        $this->Auditapprovisionnements = TableRegistry::get('Auditapprovisionnements', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Auditapprovisionnements);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
