<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ApprovisionnementsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ApprovisionnementsTable Test Case
 */
class ApprovisionnementsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ApprovisionnementsTable
     */
    public $Approvisionnements;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.approvisionnements',
        'app.fournisseurs',
        'app.produits',
        'app.ventes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Approvisionnements') ? [] : ['className' => ApprovisionnementsTable::class];
        $this->Approvisionnements = TableRegistry::get('Approvisionnements', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Approvisionnements);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
